# Agenda per la Semplificazione 2015-2017 - GdL Processi

Nel presente repository trovano posto i semi-lavorati prodotti (disegni BPMN e documentazione) dal GdL Processi per il GdL Architetture, durante le attività svolte nell'ambito dei lavori sull'interoperabilità 
promossi dal Dipartimento della Funzione Pubblica per l'attuazione dell'Agenda per la Semplificazione 2015-2017.

[Link al wiki](https://gitlab.com/pasemplice-agid/gdl-processi/wikis)

In particolare il gruppo ha provveduto alla individuazione, mappatura, analisi e disegno dei processi (con notazione **BPMN**) descritti dalle norme in materia di semplificazione per categoria di procedimenti 
amministrativi quali:

- Comunicazione;
- SCIA;
- Autorizzazione (o Domanda);

e relative combinazioni di regime

- SCIA Unica;
- SCIA Condizionata.

I disegni BPMN prodotti sono da considerarsi un .

Al momento restano da approvare:

- SCIA Condizionata;
- Azioni di refactoring sui BPMN di SCIA, Domanda, SCIA Unica.

Saranno oggetto delle prossime discussioni:

- SCIA in COMUNICA;
- Refactoring ai BPMN per la gestione delle eccezioni di processo;
- Questione Agenzie delle Imprese.